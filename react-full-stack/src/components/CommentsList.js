import React from 'react';

const CommentsList = ({ comments, key }) => (
    <>  
    {comments.map(comment => (
        <div className="comment" key="key">
            <h4>{comment.username}</h4>
            <p>{comment.text}</p>
        </div>
    ))}
    </>
)

export default CommentsList;